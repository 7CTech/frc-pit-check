import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("com.google.gms.google-services")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "ctech.frcpitcheck"
        minSdkVersion(27)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    lintOptions {
	    setAbortOnError(false)
    }
    dataBinding {
        setEnabled(true)
    }
    compileOptions {
        setSourceCompatibility(JavaVersion.VERSION_1_8)
        setTargetCompatibility(JavaVersion.VERSION_1_8)
    }
}

dependencies {
    val ankoVersion: String by extra
    val splittiesVersion: String by extra
    val splittiesSnapshotVersion: String by extra

    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib-jdk8", KotlinCompilerVersion.VERSION))
    implementation(kotlin("reflect", KotlinCompilerVersion.VERSION))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.1.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-play-services:1.1.0")

    /*implementation("org.jetbrains.anko:anko-commons:$ankoVersion")
    implementation("org.jetbrains.anko:anko:$ankoVersion")
    implementation("org.jetbrains.anko:anko-appcompat-v7-commons:$ankoVersion")
    implementation("org.jetbrains.anko:anko-constraint-layout:$ankoVersion")
    implementation("org.jetbrains.anko:anko-appcompat-v7:$ankoVersion")
    implementation("org.jetbrains.anko:anko-coroutines:$ankoVersion")*/

    implementation("androidx.appcompat:appcompat:1.0.2")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("androidx.constraintlayout:constraintlayout-solver:1.1.3")
    implementation("androidx.core:core-ktx:1.0.2")
    implementation("androidx.drawerlayout:drawerlayout:1.0.0")
    implementation("androidx.fragment:fragment-ktx:1.0.0")
    implementation("androidx.lifecycle:lifecycle-common-java8:2.0.0")
    implementation("androidx.lifecycle:lifecycle-extensions:2.0.0")
    implementation("androidx.recyclerview:recyclerview:1.0.0")
    implementation("com.google.android.material:material:1.0.0")
    implementation("com.google.firebase:firebase-core:16.0.9")
    implementation("com.google.firebase:firebase-auth:17.0.0")
    implementation("com.google.firebase:firebase-firestore:19.0.0")
    implementation("com.google.firebase:firebase-functions:17.0.0")

    /*SPLITTIES!*/
    //todo play around and see which of these i actually use
    implementation("com.louiscad.splitties:splitties-dimensions:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-material-colors:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-material-lists:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-preferences:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-resources:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-snackbar:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-systemservices:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-typesaferecyclerview:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-appcompat:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-dsl:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-dsl-appcompat:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-dsl-constraintlayout:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-dsl-coordinatorlayout:$splittiesVersion")
    debugImplementation("com.louiscad.splitties:splitties-views-dsl-ide-preview:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-dsl-material:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-dsl-recyclerview:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-material:$splittiesVersion")
    implementation("com.louiscad.splitties:splitties-views-recyclerview:$splittiesVersion")

    implementation("com.google.android.gms:play-services-auth:16.0.1")

    implementation("com.squareup.picasso:picasso:2.71828")

    testImplementation("junit:junit:4.12")
    androidTestImplementation("androidx.test.ext:junit:1.1.0")
    androidTestImplementation("androidx.test:runner:1.1.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.1.1")

    kapt("com.android.databinding:compiler:3.2.1")
}

tasks.withType<KotlinCompile>().all {
    sourceCompatibility = JavaVersion.VERSION_1_8.toString()
    targetCompatibility = JavaVersion.VERSION_1_8.toString()

    kotlinOptions {
        jvmTarget = "1.8"
        apiVersion = "1.3"
        languageVersion = "1.3"
    }
}