package ctech.frcpitcheck.local

import ctech.frcpitcheck.model.Subsystem

class LocalSubsystem(override val event: String, override val year: String, override val match: String, override val subsystem: String, override val name: String, override val owners: List<String>) : Subsystem() {
    constructor(event: String, year: String, match: String, subsystem: String, map: Map<String, Any>) : this(event, year, match, subsystem, map["name"] as String, map["owners"] as List<String>)
}