package ctech.frcpitcheck.local

import ctech.frcpitcheck.model.Match
import java.time.Instant
import java.util.Date

class LocalMatch(override val event: String, override val year: String, override val match: String, time: Date) : Match(time) {
    constructor(event: String, year: String, match: String, time: Long) : this(event, year, match, Date.from(Instant.ofEpochMilli(time)))
    constructor(event: String, year: String, match: String, map: Map<String, Any>) : this(event, year, match, Date.from(Instant.ofEpochMilli(map["time"] as Long)))
}