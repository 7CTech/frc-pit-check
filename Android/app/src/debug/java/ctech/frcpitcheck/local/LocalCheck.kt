package ctech.frcpitcheck.local

import ctech.frcpitcheck.CheckType
import ctech.frcpitcheck.model.Check

class LocalCheck(override val event: String, override val year: String, override val match: String, override val subsystem: String, override val check: String,
                 override val checkedBy: List<String>, override val description: String, override val name: String, result: String, override val type: CheckType) : Check(result) {
    constructor(event: String, year: String, match: String, subsystem: String, check: String, map: Map<String, Any>) : this(event, year, match, subsystem, check, map["checked_by"] as List<String>, map["description"] as String, map["name"] as String, map["result"] as String, CheckType.fromInt((map["type"] as Long).toInt()))
}