package ctech.frcpitcheck.local

import ctech.frcpitcheck.interfaces.LoginUser

class LocalUser : LoginUser {
    override var hasAdmin: Boolean = false
    override var uid: String = ""

    override var displayName: String = ""
    override var email: String = ""

    override suspend fun refresh() {

    }
}