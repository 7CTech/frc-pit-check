package ctech.frcpitcheck.local

import ctech.frcpitcheck.model.Year

class LocalYear(override val event: String, override val year: String, robotKey: String) : Year() {
    constructor(event: String, match: String, map: Map<String, Any>) : this(event, match, map["robot_key"] as String) {
        this.robotKey = robotKey
    }
}