package ctech.frcpitcheck.local

import ctech.frcpitcheck.model.Event
import java.time.Instant
import java.util.Date

class LocalEvent(override val event: String, override val name: String, override val startTime: Date, override val endTime: Date): Event() {
    constructor(event: String, name: String, startTime: Long, endTime: Long) : this(event, name, Date.from(Instant.ofEpochMilli(startTime)), Date.from(Instant.ofEpochMilli(endTime)))
    constructor(event: String, map: Map<String, Any>) : this(event, map["name"] as String, Date.from(Instant.ofEpochMilli(map["start"] as Long)), Date.from(Instant.ofEpochMilli(map["end"] as Long)))
}