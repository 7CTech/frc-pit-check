package ctech.frcpitcheck.local

import com.google.gson.JsonParser
import ctech.frcpitcheck.CheckType
import ctech.frcpitcheck.model.*

object LocalSource {
    private const val json = "{\n" +
            "  \"events\": {\n" +
            "    \"CASJ\": {\n" +
            "      \"name\": \"Silicon Valley Region\",\n" +
            "      \"start\": \"1546297980195\",\n" +
            "      \"end\": \"1546557180195\",\n" +
            "      \"years\": {\n" +
            "        \"2018\": {\n" +
            "          \"robot_key\": \"2018\",\n" +
            "          \"matches\": {\n" +
            "            \"1\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"intake\": {\n" +
            "                  \"name\": \"Intake\",\n" +
            "                  \"owners\": [\n" +
            "                    \"12n4iu12\",\n" +
            "                    \"123ni123\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"0\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            },\n" +
            "            \"2\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"shooter\": {\n" +
            "                  \"name\": \"Shooter\",\n" +
            "                  \"owners\": [\n" +
            "                    \"1231231239\",\n" +
            "                    \"1n2u12i93\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"0\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            }\n" +
            "          }\n" +
            "        }\n" +
            "      }\n" +
            "    },\n" +
            "    \"AUSC\": {\n" +
            "      \"name\": \"Southern Cross Regional\",\n" +
            "      \"start\": \"1546297980195\",\n" +
            "      \"end\": \"1546557180195\",\n" +
            "      \"years\": {\n" +
            "        \"2017\": {\n" +
            "          \"robot_key\": \"2017\",\n" +
            "          \"matches\": {\n" +
            "            \"3\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"drive\": {\n" +
            "                  \"name\": \"Drivetrain\",\n" +
            "                  \"owners\": [\n" +
            "                    \"1n9u12n39123\",\n" +
            "                    \"1239nu1239\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"1\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            },\n" +
            "            \"4\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"end\": {\n" +
            "                  \"name\": \"Endgame\",\n" +
            "                  \"owners\": [\n" +
            "                    \"12n39123\",\n" +
            "                    \"12n39123\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"1\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            }\n" +
            "          }\n" +
            "        },\n" +
            "        \"2018\": {\n" +
            "          \"robot_key\": \"2018\",\n" +
            "          \"matches\": {\n" +
            "            \"5\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"shooter\": {\n" +
            "                  \"name\": \"Shooter\",\n" +
            "                  \"owners\": [\n" +
            "                    \"134123\",\n" +
            "                    \"1oij912\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"1\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            },\n" +
            "            \"6\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"intake\": {\n" +
            "                  \"name\": \"Intake\",\n" +
            "                  \"owners\": [\n" +
            "                    \"123\",\n" +
            "                    \"niu123\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"1\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            }\n" +
            "          }\n" +
            "        },\n" +
            "        \"2019\": {\n" +
            "          \"robot_key\": \"2019\",\n" +
            "          \"matches\": {\n" +
            "            \"7\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"drive\": {\n" +
            "                  \"name\": \"Drivetrain\",\n" +
            "                  \"owners\": [\n" +
            "                    \"12n391293123\",\n" +
            "                    \"1203j123\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"1\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            },\n" +
            "            \"8\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"shooter\": {\n" +
            "                  \"name\": \"Shooter\",\n" +
            "                  \"owners\": [\n" +
            "                    \"134123\",\n" +
            "                    \"1oij912\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"1\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            },\n" +
            "            \"9\": {\n" +
            "              \"time\": \"1546307980195\",\n" +
            "              \"subsystems\": {\n" +
            "                \"gear\": {\n" +
            "                  \"name\": \"Gear\",\n" +
            "                  \"owners\": [\n" +
            "                    \"12o3n123\",\n" +
            "                    \"123noj123\"\n" +
            "                  ],\n" +
            "                  \"checks\": {\n" +
            "                    \"0\": {\n" +
            "                      \"name\": \"Clear chain path\",\n" +
            "                      \"description\": \"Ensure that the chain on both sides of the drivetrain is clear of any obstructions, such as nuts, zipties, or rivet heads\",\n" +
            "                      \"result\": \"1\",\n" +
            "                      \"type\": 0\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              }\n" +
            "            }\n" +
            "          }\n" +
            "        }\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}\n"

    private val parsed = JsonParser().parse(json)

    fun getEvents(): List<Event> {
        return parsed.asJsonObject["events"].asJsonObject.entrySet().map {
            val event = it.value.asJsonObject
            LocalEvent(it.key, event.get("name").asString, event.get("start").asLong, event.get("end").asLong)
        }
    }

    fun getYears(event: String): List<Year> {
        return parsed.asJsonObject["events"].asJsonObject[event].asJsonObject["years"].asJsonObject.entrySet().map {
            val year = it.value.asJsonObject
            LocalYear(event, it.key, year.get("robot_key").asString)
        }
    }

    fun getMatches(event: String, year: String): List<Match> {
        return parsed.asJsonObject["events"].asJsonObject[event].asJsonObject["years"].asJsonObject[year].asJsonObject["matches"].asJsonObject.entrySet().map {
            val match = it.value.asJsonObject
            LocalMatch(event, year, it.key, match.get("time").asLong)
        }
    }

    fun getSubsystems(event: String, year: String, match: String): List<Subsystem> {
        return parsed.asJsonObject["events"].asJsonObject[event].asJsonObject["years"].asJsonObject[year].asJsonObject["matches"].asJsonObject[match].asJsonObject["subsystems"].asJsonObject.entrySet().map {
            val subsystem = it.value.asJsonObject
            LocalSubsystem(event, year, match, it.key, subsystem.get("name").asString, subsystem.get("owners").asJsonArray.map { e -> e.asString}) //huh
        }
    }

    fun getChecks(event: String, year: String, match: String, subsystem: String): List<Check> {
        return parsed.asJsonObject["events"].asJsonObject[event].asJsonObject["years"].asJsonObject[year].asJsonObject["matches"].asJsonObject[match].asJsonObject["subsystems"].asJsonObject[subsystem].asJsonObject["checks"].asJsonObject.entrySet().map {
            val check = it.value.asJsonObject
            LocalCheck(event, year, match, subsystem, it.key, check.get("checkedBy").asJsonArray.map { e -> e.asString }, check.get("description").asString, check.get("name").asString, check.get("result").asString, CheckType.fromInt((check["type"] as Long).toInt())) //huh
        }
    }
}