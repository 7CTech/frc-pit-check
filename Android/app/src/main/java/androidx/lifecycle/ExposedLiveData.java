package androidx.lifecycle;

import androidx.arch.core.executor.ArchTaskExecutor;

import java.lang.reflect.Field;

public class ExposedLiveData <T> extends LiveData<T> {

    public ExposedLiveData(T baseValue) {
        setValue(baseValue);
    }

    protected void change(String methodName) throws NoSuchFieldException, IllegalAccessException {
        assertMainThread(methodName);
        Field versionField = LiveData.class.getDeclaredField("mVersion");
        versionField.setAccessible(true);
        versionField.set(this, versionField.getInt(this) + 1);
        dispatchingValue(null);
    }

    private static void assertMainThread(String methodName) {
        if (!ArchTaskExecutor.getInstance().isMainThread()) {
            throw new IllegalStateException("Cannot invoke " + methodName + " on a background"
                    + " thread");
        }
    }
}
