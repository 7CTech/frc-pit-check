package ctech.frcpitcheck

import androidx.databinding.Observable
import androidx.lifecycle.ExposedLiveData
import kotlin.contracts.contract

/**
 * Returns the index of the first element matching the given [predicate], or `-1` if no such element was found.
 */
public inline fun <T> Iterable<T>.findIndex(predicate: (T) -> Boolean): Int {
    this.forEachIndexed { index, element -> if (predicate(element)) return index }
    return -1
}

public inline fun Observable.addOnPropertyChangedCallback(crossinline callback: (sender: Observable, propertyId: Int) -> Unit) {
    this.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            callback(sender, propertyId)
        }
    })
}
