package ctech.frcpitcheck

import java.time.Duration
import java.time.LocalDateTime
import java.time.Period
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

object Util {
    fun getTimeBetweenString(now: LocalDateTime, other: LocalDateTime, smallest: TimeUnit = TimeUnit.MINUTES, afterResult: String = ""): Pair<Boolean, String> {
        if (now.isAfter(other)) return false to afterResult
        val dateDiff = Period.between(now.toLocalDate(), other.toLocalDate())
        val timeDiff = Duration.between(now.toLocalTime(), other.toLocalTime())
        return true to when {
            dateDiff.years > 1 -> "${dateDiff.years} years"
            dateDiff.years == 1 -> "1 year"
            dateDiff.months > 1 -> "${dateDiff.months} months"
            dateDiff.months == 1 -> "1 month"
            dateDiff.days > 1 -> "${dateDiff.days} days"
            dateDiff.days == 1 -> "1 day"
            else -> {
                val hours = timeDiff.toHours().toInt()
                if (hours >= 1) {
                    return true to if (hours > 1) "$hours hours" else "1 hour"
                }
                val minutes = timeDiff.toMinutes().toInt()
                if (minutes >= 1) {
                    return true to if (minutes > 1) "$minutes minutes" else "1 minute"
                }
                if (timeDiff.seconds > 1) return true to "${timeDiff.seconds} seconds"
                else if (timeDiff.seconds.toInt() == 1) return true to "1 second"
                return false to "OwO"
            }
        }
    }

    private val intervalExecutor = Executors.newSingleThreadScheduledExecutor()

    fun executeAtInterval(block: () -> Unit, interval: Long, unit: TimeUnit): ScheduledFuture<*> = intervalExecutor.scheduleAtFixedRate(block, 0, interval, unit)
}