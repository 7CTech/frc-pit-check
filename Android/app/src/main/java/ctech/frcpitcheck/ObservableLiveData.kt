package ctech.frcpitcheck

import androidx.databinding.BaseObservable
import androidx.databinding.Observable
import androidx.lifecycle.ExposedLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ObservableLiveData<T : BaseObservable>(baseValue: T) : ExposedLiveData<T>(baseValue) {
    override fun setValue(value: T) {
        super.setValue(value)
        if (hasActiveObservers()) value.addOnPropertyChangedCallback(callback)
    }

    override fun onActive() {
        value?.addOnPropertyChangedCallback(callback)
    }

    override fun onInactive() {
        GlobalScope.launch {
            delay(2000) // don't kill and re-add if we're just rotating - retain same instance of listener
            if (!hasActiveObservers()) {
                value?.removeOnPropertyChangedCallback(callback)
            }
        }
    }

    private val callback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            change("callback")
        }
    }
}