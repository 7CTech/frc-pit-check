package ctech.frcpitcheck.interfaces

import ctech.frcpitcheck.fragments.*

interface AllFragmentListener : EventsFragment.EventFragmentInteractionListener,
        YearsFragment.YearsFragmentInteractionListener,
        MatchesFragment.MatchesFragmentInteractionListener,
        SubsystemsFragment.SubsystemsFragmentInteractionListener,
        ChecksFragment.ChecksFragmentInteractionListener {
}