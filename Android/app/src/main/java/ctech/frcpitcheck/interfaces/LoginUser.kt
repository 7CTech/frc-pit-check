package ctech.frcpitcheck.interfaces

interface LoginUser {
    val hasAdmin: Boolean
    val uid: String

    val displayName: String
    val email: String

    suspend fun refresh()
}