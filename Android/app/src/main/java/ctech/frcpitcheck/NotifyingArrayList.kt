package ctech.frcpitcheck

import java.util.function.Predicate

class NotifyingArrayList<T> : ArrayList<T> {
    private val mNotify: () -> Unit
    constructor(notify: () -> Unit, initialCapacity: Int): super(initialCapacity) {
        mNotify = notify
    }
    constructor(notify: () -> Unit) : super() {
        mNotify = notify
    }
    constructor(notify: () -> Unit, c: Collection<T>) : super(c) {
        mNotify = notify
    }

    override fun add(element: T): Boolean {
        val r = super.add(element)
        mNotify()
        return r
    }

    override fun add(index: Int, element: T) {
        super.add(index, element)
        mNotify()
    }

    override fun addAll(elements: Collection<T>): Boolean {
        val r = super.addAll(elements)
        mNotify()
        return r
    }

    override fun addAll(index: Int, elements: Collection<T>): Boolean {
        val r = super.addAll(index, elements)
        mNotify()
        return r
    }

    override fun remove(element: T): Boolean {
        val r = super.remove(element)
        mNotify()
        return r
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        val r = super.removeAll(elements)
        mNotify()
        return r
    }

    override fun removeAt(index: Int): T {
        val r = super.removeAt(index)
        mNotify()
        return r
    }

    override fun removeIf(filter: Predicate<in T>): Boolean {
        val r = super.removeIf(filter)
        mNotify()
        return r
    }

    override fun removeRange(fromIndex: Int, toIndex: Int) {
        super.removeRange(fromIndex, toIndex)
        mNotify()
    }

    override fun set(index: Int, element: T): T {
        val r = super.set(index, element)
        mNotify()
        return r
    }

    override fun get(index: Int): T {
        val r = super.get(index)
        mNotify()
        return r
    }
}