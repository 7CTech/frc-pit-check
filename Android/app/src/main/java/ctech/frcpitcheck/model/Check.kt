package ctech.frcpitcheck.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ctech.frcpitcheck.BR
import ctech.frcpitcheck.CheckType

abstract class Check(result: String) : BaseObservable() {
    abstract val event: String
    abstract val year: String
    abstract val match: String
    abstract val subsystem: String
    abstract val check: String

    abstract val checkedBy: List<String>
    abstract val description: String
    abstract val name: String
    var result: String = result
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.result)
        }
    abstract val type: CheckType

    override fun equals(other: Any?): Boolean {
        return other is Check &&
                this.event == other.event &&
                this.year == other.year &&
                this.match == other.match &&
                this.subsystem == other.subsystem &&
                this.check == other.check &&
                this.checkedBy == other.checkedBy &&
                this.description == other.description &&
                this.name == other.name &&
                this.result == other.result &&
                this.type == other.type
    }

    override fun hashCode(): Int {
        var result = event.hashCode()
        result = 31 * result + year.hashCode()
        result = 31 * result + match.hashCode()
        result = 31 * result + subsystem.hashCode()
        result = 31 * result + check.hashCode()
        result = 31 * result + checkedBy.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + result.hashCode()
        result = 31 * result + type.hashCode()
        return result
    }
}