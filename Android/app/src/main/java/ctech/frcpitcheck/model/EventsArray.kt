package ctech.frcpitcheck.model

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import ctech.frcpitcheck.impl.firebase.FirebaseEventImpl

class EventsArrayFirebase : FirebaseModelArray<Event>() {
    override val ref: CollectionReference = FirebaseFirestore.getInstance().collection("events")
    override val snapshotListener: EventListener<QuerySnapshot> = EventListener { snapshot, _ ->
        value = snapshot?.documents?.map { FirebaseEventImpl.fromFirebaseMap(it.reference, it.data!!)  }
    }
}
/*
class EventsArrayLocal : LiveData<List<Event>>() {
    init {
        value = LocalSource.getEvents()
    }
}
*/