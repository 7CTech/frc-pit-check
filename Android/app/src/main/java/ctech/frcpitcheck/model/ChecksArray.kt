package ctech.frcpitcheck.model

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import ctech.frcpitcheck.ObservableLiveData
import ctech.frcpitcheck.impl.firebase.FirebaseCheckImpl

class ChecksArrayFirebase(event: String, year: String, match: String, subsystem: String) : FirebaseModelArray<LiveData<Check>>() {
    override val ref: CollectionReference = FirebaseFirestore.getInstance().collection("events").document(event).collection("years").document(year).collection("matches").document(match).collection("subsystems").document(subsystem).collection("checks")
    override val snapshotListener: EventListener<QuerySnapshot> = EventListener { snapshot, _ ->

        value = snapshot?.documents?.map { ObservableLiveData(FirebaseCheckImpl.fromFirebaseMap(event, year, match, subsystem, it.reference, it.data!!)) }
    }
}
/*
class ChecksArrayLocal(event: String, year: String, match: String, subsystem: String) : LiveData<List<LiveData<Check>>>() {
    init {
        value = LocalSource.getChecks(event, year, match, subsystem).map { ObservableLiveData(it) }
    }
}*/