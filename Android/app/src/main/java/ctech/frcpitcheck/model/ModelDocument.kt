package ctech.frcpitcheck.model

import androidx.databinding.BaseObservable
import com.google.firebase.firestore.DocumentReference

abstract class ModelDocument(protected val ref: DocumentReference) : BaseObservable() {
    abstract val path: String
}