package ctech.frcpitcheck.model

import java.util.Date

abstract class Event {
    abstract val event: String
    abstract val name: String
    abstract val startTime: Date
    abstract val endTime: Date

    fun prevMatch(): String {
        return ""
    }

    override fun toString(): String {
        val sb = StringBuilder("<Event>$name ($event): $startTime - $endTime [")
        /*years.value?.forEachIndexed { index, year ->
            sb.append(year)
            if (index != years.value!!.size - 1) sb.append(", ")
        }*/
        return sb.append("]").toString()
    }

    override fun equals(other: Any?): Boolean {
        return other is Event &&
                this.event == other.event &&
                this.name == other.name &&
                this.startTime == other.startTime &&
                this.endTime == other.endTime

    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + startTime.hashCode()
        result = 31 * result + endTime.hashCode()
        result = 31 * result + event.hashCode()
        return result
    }
}
