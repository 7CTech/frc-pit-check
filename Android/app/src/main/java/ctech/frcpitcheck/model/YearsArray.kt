package ctech.frcpitcheck.model

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import ctech.frcpitcheck.ObservableLiveData
import ctech.frcpitcheck.impl.firebase.FirebaseYearImpl

class YearsArrayFirebase(event: String) : FirebaseModelArray<Year>() {
    override val ref: CollectionReference = FirebaseFirestore.getInstance().collection("events").document(event).collection("years")
    override val snapshotListener: EventListener<QuerySnapshot> = EventListener { snapshot, _ ->
        value = snapshot?.documents?.map { FirebaseYearImpl.fromFirebaseMap(event, it.reference, it.data!!)  }
    }
}
/*
class YearsArrayLocal(event: String) : LiveData<List<Year>>() {
    init {
        value = LocalSource.getYears(event)
    }
}*/