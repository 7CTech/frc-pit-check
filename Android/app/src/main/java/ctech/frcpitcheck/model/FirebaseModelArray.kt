package ctech.frcpitcheck.model

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

//maybe make this LiveData<List<ScheduledLiveData<T>>>
abstract class FirebaseModelArray <T>: LiveData<List<T>>() {
    protected abstract val ref: CollectionReference
    private lateinit var snapshotListenerRegistration: ListenerRegistration
    protected abstract val snapshotListener: EventListener<QuerySnapshot>

    init {
        value = listOf()
    }

    override fun onActive() {
        snapshotListenerRegistration = ref.addSnapshotListener(snapshotListener)
    }

    override fun onInactive() {
        GlobalScope.launch {
            delay(2000) // don't kill and re-add if we're just rotating - retain same instance of listener
            if (!hasActiveObservers()) {
                snapshotListenerRegistration.remove()
            }
        }
    }
}