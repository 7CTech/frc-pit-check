package ctech.frcpitcheck.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ctech.frcpitcheck.BR
import java.util.Date

abstract class Match (time: Date) : BaseObservable() {
    abstract val event: String
    abstract val year: String
    abstract val match: String
    var time: Date = time
        @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.time)
        }

    override fun equals(other: Any?): Boolean {
        return other is Match &&
                this.event == other.event &&
                this.year == other.year &&
                this.match == other.match &&
                this.time == other.time
    }

    override fun hashCode(): Int {
        var result = event.hashCode()
        result = 31 * result + year.hashCode()
        result = 31 * result + match.hashCode()
        result = 31 * result + time.hashCode()
        return result
    }
}