package ctech.frcpitcheck.model

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import ctech.frcpitcheck.impl.firebase.FirebaseSubsystemImpl

class SubsystemsArrayFirebase(event: String, year: String, match: String) : FirebaseModelArray<Subsystem>() {
    override val ref: CollectionReference = FirebaseFirestore.getInstance().collection("events").document(event).collection("years").document(year).collection("matches").document(match).collection("subsystems")
    override val snapshotListener: EventListener<QuerySnapshot> = EventListener { snapshot, _ ->
        value = snapshot?.documents?.map { FirebaseSubsystemImpl.fromFirebaseMap(event, year, match, it.reference, it.data!!) }
    }
}

/*
class SubsystemsArrayLocal(event: String, year: String, match: String) : LiveData<List<Subsystem>>() {
    init {
        value = LocalSource.getSubsystems(event, year, match)
    }
}*/