package ctech.frcpitcheck.model

abstract class Subsystem {
    abstract val event: String
    abstract val year: String
    abstract val match: String
    abstract val subsystem: String
    abstract val name: String
    abstract val owners: List<String>

    override fun equals(other: Any?): Boolean {
        return other is Subsystem &&
                this.event == other.event &&
                this.year == other.year &&
                this.match == other.match &&
                this.subsystem == other.subsystem &&
                this.name == other.name &&
                this.owners == other.owners
    }

    override fun hashCode(): Int {
        var result = event.hashCode()
        result = 31 * result + year.hashCode()
        result = 31 * result + match.hashCode()
        result = 31 * result + subsystem.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + owners.hashCode()
        return result
    }
}