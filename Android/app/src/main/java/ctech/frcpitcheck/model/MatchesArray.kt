package ctech.frcpitcheck.model

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import ctech.frcpitcheck.ObservableLiveData
import ctech.frcpitcheck.impl.firebase.FirebaseMatchImpl

class MatchesArrayFirebase(event: String, year: String) : FirebaseModelArray<LiveData<Match>>() {
    override val ref: CollectionReference = FirebaseFirestore.getInstance().collection("events").document(event).collection("years").document(year).collection("matches")
    override val snapshotListener: EventListener<QuerySnapshot> = EventListener { snapshot, _ ->
        value = snapshot?.documents?.map { ObservableLiveData(FirebaseMatchImpl.fromFirebaseMap(event, year, it.reference, it.data!!)) }
    }
}

/*
class MatchesArrayLocal(event: String, year: String) : LiveData<List<LiveData<Match>>>() {
    init {
        value = LocalSource.getMatches(event, year).map { ObservableLiveData(it) }
    }
}*/