package ctech.frcpitcheck.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ctech.frcpitcheck.BR

abstract class Year : BaseObservable() {
    abstract val event: String
    abstract val year: String

    var robotKey: String = ""
        @Bindable get() = field
        set(value) {
            notifyPropertyChanged(BR.robotKey)
            field = value
        }

    override fun equals(other: Any?): Boolean {
        return other is Year &&
                this.event == other.event &&
                this.year == other.year &&
                this.robotKey == other.robotKey
    }

    override fun hashCode(): Int {
        var result = event.hashCode()
        result = 31 * result + year.hashCode()
        result = 31 * result + robotKey.hashCode()
        return result
    }
}