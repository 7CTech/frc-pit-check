package ctech.frcpitcheck

import androidx.lifecycle.LiveData

class ArrayListLiveData<T> : LiveData<NotifyingArrayList<T>>() {
    init {
        value = NotifyingArrayList(::change)
    }

    private fun change() {
        LiveData::class.java.getDeclaredMethod("assertMainThread", String::class.java).apply { isAccessible = true }.invoke(this, "add")
        LiveData::class.java.getDeclaredField("mVersion").apply { isAccessible = true }.let { it.set(this, it.getInt(this@ArrayListLiveData) + 1) }
        LiveData::class.java.declaredMethods.find { it.name == "dispatchingValue" }?.apply { isAccessible=true }?.invoke(this, null)
    }
}