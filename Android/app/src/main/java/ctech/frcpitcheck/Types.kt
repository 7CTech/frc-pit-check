package ctech.frcpitcheck

enum class PermissionLevel(val index: Int) {
    NONE(0),
    READ(1),

    NOTIFY(7),
    DRIVE(8),
    PIT(9),
    ADMIN(10);

    companion object {
        fun fromInt(index: Int): PermissionLevel {
            return when (index) {
                0 -> NONE
                1 -> READ

                7 -> NOTIFY
                8 -> DRIVE
                9 -> PIT
                10 -> ADMIN
                else -> NONE
            }
        }
    }
}

 enum class CheckType(val index: Int) {
    UNKNOWN(0),
    PASS_FAIL(1),
    TICKBOX(2),
    COMMENT(3);

    companion object {
        @JvmStatic
        fun fromInt(index: Int): CheckType {
            return when(index) {
                1 -> PASS_FAIL
                2 -> TICKBOX
                3 -> COMMENT

                else -> UNKNOWN
            }
        }
    }
}