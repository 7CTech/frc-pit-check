package ctech.frcpitcheck.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ctech.frcpitcheck.model.Match
import ctech.frcpitcheck.model.MatchesArrayFirebase

abstract class MatchesViewModel : ViewModel() {
    abstract val matches: LiveData<List<LiveData<Match>>>
}

/*
class MatchesLocalViewModel constructor(event: String, year: String) : MatchesViewModel() {
    override val matches = MatchesArrayLocal(event, year)
}

class MatchesLocalViewModelFactory constructor(private val event: String, private val year: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MatchesLocalViewModel(event, year) as T //yes
    }
}*/

class MatchesFirebaseViewModel constructor(event: String, year: String) : MatchesViewModel() {
    override val matches = MatchesArrayFirebase(event, year)
}

class MatchesFirebaseViewModelFactory constructor(private val event: String, private val year: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MatchesFirebaseViewModel(event, year) as T //yes
    }
}