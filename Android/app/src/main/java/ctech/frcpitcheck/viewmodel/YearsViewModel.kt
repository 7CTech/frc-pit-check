package ctech.frcpitcheck.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ctech.frcpitcheck.model.Year
import ctech.frcpitcheck.model.YearsArrayFirebase

abstract class YearsViewModel : ViewModel() {
    abstract val years: LiveData<List<Year>>
}

/*
class YearsLocalViewModel constructor(event: String): YearsViewModel() {
    override val years = YearsArrayLocal(event)
}

class YearsLocalViewModelFactory constructor(event: String): ViewModelProvider.Factory {
    private var mEvent: String = event
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return YearsLocalViewModel(mEvent) as T //yes
    }
}*/

class YearsFirebaseViewModel constructor(event: String): YearsViewModel() {
    override val years = YearsArrayFirebase(event)
}

class YearsFirebaseViewModelFactory constructor(private val event: String): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return YearsFirebaseViewModel(event) as T //yes
    }
}