package ctech.frcpitcheck.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ctech.frcpitcheck.model.Event
import ctech.frcpitcheck.model.EventsArrayFirebase


abstract class EventsViewModel : ViewModel() {
    abstract val events: LiveData<List<Event>>
}

/*
class EventsLocalViewModel : EventsViewModel() {
    override val events = EventsArrayLocal()
}

class EventsLocalViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EventsLocalViewModel() as T //yes
    }
}*/

class EventsFirebaseViewModel : EventsViewModel() {
    override val events = EventsArrayFirebase()
}

class EventsFirebaseViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EventsFirebaseViewModel() as T //yes
    }
}