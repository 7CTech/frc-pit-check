package ctech.frcpitcheck.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ctech.frcpitcheck.model.Check
import ctech.frcpitcheck.model.ChecksArrayFirebase

abstract class ChecksViewModel : ViewModel() {
    abstract val checks: LiveData<List<LiveData<Check>>>
}

/*
class ChecksLocalViewModel constructor(event: String, year: String, match: String, subsystem: String) : ChecksViewModel() {
    override val checks = ChecksArrayLocal(event, year, match, subsystem)
}

class ChecksLocalViewModelFactory constructor(private val event: String, private val year: String, private val match: String, private val subsystem: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChecksLocalViewModel(event, year, match, subsystem) as T //yes
    }
}*/

class ChecksFirebaseViewModel constructor(event: String, year: String, match: String, subsystem: String) : ChecksViewModel() {
    override val checks = ChecksArrayFirebase(event, year, match, subsystem)
}

class ChecksFirebaseViewModelFactory constructor(private val event: String, private val year: String, private val match: String, private val subsystem: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChecksFirebaseViewModel(event, year, match, subsystem) as T //yes
    }
}