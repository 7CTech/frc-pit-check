package ctech.frcpitcheck.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ctech.frcpitcheck.model.Subsystem
import ctech.frcpitcheck.model.SubsystemsArrayFirebase

abstract class SubsystemsViewModel : ViewModel() {
    abstract val subsystems: LiveData<List<Subsystem>>
}

/*
class SubsystemsLocalViewModel constructor(event: String, year: String, match: String) : SubsystemsViewModel() {
    override val subsystems = SubsystemsArrayLocal(event, year, match)
}

class SubsystemsLocalViewModelFactory constructor(private val event: String, private val year: String, private val match: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SubsystemsLocalViewModel(event, year, match) as T //yes
    }
}*/

class SubsystemsFirebaseViewModel constructor(event: String, year: String, match: String) : SubsystemsViewModel() {
    override val subsystems = SubsystemsArrayFirebase(event, year, match)
}

class SubsystemsFirebaseViewModelFactory constructor(private val event: String, private val year: String, private val match: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SubsystemsFirebaseViewModel(event, year, match) as T //yes
    }
}