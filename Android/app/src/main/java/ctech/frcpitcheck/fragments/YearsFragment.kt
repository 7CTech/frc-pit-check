package ctech.frcpitcheck.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import ctech.frcpitcheck.model.Year
import ctech.frcpitcheck.ui.RecyclerViewFragmentDefaultUi
import ctech.frcpitcheck.ui.fragmentitems.YearItemUi
import ctech.frcpitcheck.viewmodel.YearsFirebaseViewModelFactory
import ctech.frcpitcheck.viewmodel.YearsViewModel

class YearsFragment : Fragment() {
    private var listener: YearsFragmentInteractionListener? = null
    private lateinit var mEvent: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mEvent = arguments?.getString("event")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = RecyclerViewFragmentDefaultUi(inflater.context).root
        with(view) {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = YearsRecyclerViewAdapter(listener)
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is YearsFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement YearsFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface YearsFragmentInteractionListener {
        fun onSelectYear(year: Year?)
    }

    companion object {
        @JvmStatic
        fun newInstance(event: String): YearsFragment {
            val args = Bundle().apply {
                putString("event", event)
            }
            return YearsFragment().apply {
                arguments = args
            }
        }
    }

    inner class YearsRecyclerViewAdapter(
            private val mListener: YearsFragment.YearsFragmentInteractionListener?)
        : RecyclerView.Adapter<YearsRecyclerViewAdapter.ViewHolder>() {


        private val mOnClickListener: View.OnClickListener
        private var mYears: List<Year>

        init {
            mOnClickListener = View.OnClickListener { v ->
                val year = v.tag as Year
                // Notify the active callbacks interface (the activity, if the fragment is attached to
                // one) that an item has been selected.
                mListener?.onSelectYear(year)
            }

            ViewModelProviders.of(this@YearsFragment, YearsFirebaseViewModelFactory(mEvent)).get(YearsViewModel::class.java).years.let { list ->
                mYears = list.value!!
                list.observe(this@YearsFragment, androidx.lifecycle.Observer { years ->
                    DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                        override fun getOldListSize(): Int {
                            return mYears.size
                        }

                        override fun getNewListSize(): Int {
                            return years.size
                        }

                        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mYears[oldItemPosition].event == years[newItemPosition].event &&
                                    mYears[oldItemPosition].year == years[newItemPosition].year
                        }

                        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mYears[oldItemPosition] == years[newItemPosition]
                        }
                    }).also { mYears = list.value!! }.dispatchUpdatesTo(this@YearsRecyclerViewAdapter)
                })
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val ui = YearItemUi(parent.context)
            return ViewHolder(ui.root, ui)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val year = mYears[position]
            holder.ui.year = year.year

            with(holder.view) {
                tag = year
                setOnClickListener(mOnClickListener)
            }
        }

        override fun getItemCount(): Int = mYears.size

        inner class ViewHolder(val view: View, val ui: YearItemUi) : RecyclerView.ViewHolder(view)
    }
}
