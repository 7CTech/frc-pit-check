package ctech.frcpitcheck.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import ctech.frcpitcheck.model.Subsystem
import ctech.frcpitcheck.ui.RecyclerViewFragmentDefaultUi
import ctech.frcpitcheck.ui.fragmentitems.SubsystemItemUi
import ctech.frcpitcheck.viewmodel.SubsystemsFirebaseViewModelFactory
import ctech.frcpitcheck.viewmodel.SubsystemsViewModel

class SubsystemsFragment : Fragment() {
    private var listener: SubsystemsFragmentInteractionListener? = null
    private lateinit var mEvent: String
    private lateinit var mYear: String
    private lateinit var mMatch: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mEvent = arguments?.getString("event")!!
        mYear = arguments?.getString("year")!!
        mMatch = arguments?.getString("match")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = RecyclerViewFragmentDefaultUi(inflater.context).root
        with(view) {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = SubsystemsRecyclerViewAdapter(listener)
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SubsystemsFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement SubsystemsFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface SubsystemsFragmentInteractionListener {
        fun onSelectSubsystem(subsystem: Subsystem?)
    }

    companion object {
        @JvmStatic
        fun newInstance(event: String, year: String, match: String): SubsystemsFragment {
            val args = Bundle().apply {
                putString("event", event)
                putString("year", year)
                putString("match", match)
            }
            return SubsystemsFragment().apply {
                arguments = args
            }
        }
    }

    inner class SubsystemsRecyclerViewAdapter(
            private val mListener: SubsystemsFragment.SubsystemsFragmentInteractionListener?)
        : RecyclerView.Adapter<SubsystemsRecyclerViewAdapter.ViewHolder>() {


        private val mOnClickListener: View.OnClickListener
        private var mSubsystems: List<Subsystem>

        init {
            mOnClickListener = View.OnClickListener { v ->
                val subsystem = v.tag as Subsystem
                // Notify the active callbacks interface (the activity, if the fragment is attached to
                // one) that an item has been selected.
                mListener?.onSelectSubsystem(subsystem)
            }

            ViewModelProviders.of(this@SubsystemsFragment, SubsystemsFirebaseViewModelFactory(mEvent, mYear, mMatch)).get(SubsystemsViewModel::class.java).subsystems.let { list ->
                mSubsystems = list.value!!
                list.observe(this@SubsystemsFragment, androidx.lifecycle.Observer { subsystems ->
                    DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                        override fun getOldListSize(): Int {
                            return mSubsystems.size
                        }

                        override fun getNewListSize(): Int {
                            return subsystems.size
                        }

                        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mSubsystems[oldItemPosition].event == subsystems[newItemPosition].event &&
                                    mSubsystems[oldItemPosition].year == subsystems[newItemPosition].year &&
                                    mSubsystems[oldItemPosition].match == subsystems[newItemPosition].match &&
                                    mSubsystems[oldItemPosition].subsystem == subsystems[newItemPosition].subsystem
                        }

                        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mSubsystems[oldItemPosition] == subsystems[newItemPosition]
                        }
                    }).also { mSubsystems = list.value!! }.dispatchUpdatesTo(this@SubsystemsRecyclerViewAdapter)
                })
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val ui = SubsystemItemUi(parent.context)
            return ViewHolder(ui.root, ui)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val subsystem = mSubsystems[position]
            holder.ui.subsystemName = subsystem.name

            with(holder.view) {
                tag = subsystem
                setOnClickListener(mOnClickListener)
            }
        }

        override fun getItemCount(): Int = mSubsystems.size

        inner class ViewHolder(val view: View, val ui: SubsystemItemUi) : RecyclerView.ViewHolder(view)
    }
}
