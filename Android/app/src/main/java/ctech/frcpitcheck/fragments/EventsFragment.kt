package ctech.frcpitcheck.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import ctech.frcpitcheck.model.Event
import ctech.frcpitcheck.ui.RecyclerViewFragmentDefaultUi
import ctech.frcpitcheck.ui.fragmentitems.EventItemUi
import ctech.frcpitcheck.viewmodel.EventsFirebaseViewModelFactory
import ctech.frcpitcheck.viewmodel.EventsViewModel
import java.time.LocalDateTime

class EventsFragment private constructor(): Fragment() {
    private var mListener: EventFragmentInteractionListener? = null
    private lateinit var mFirestore: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFirestore = FirebaseFirestore.getInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = RecyclerViewFragmentDefaultUi(inflater.context).root
        with(view) {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = EventsRecyclerViewAdapter(mListener)
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is EventFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface EventFragmentInteractionListener {
        fun onSelectEvent(event: Event?)
    }

    companion object {
        @JvmStatic
        fun newInstance() = EventsFragment()
    }

    inner class EventsRecyclerViewAdapter(
            private val mListener: EventFragmentInteractionListener?)
        : RecyclerView.Adapter<EventsRecyclerViewAdapter.ViewHolder>() {

        private val mOnClickListener: View.OnClickListener

        private lateinit var context: Context
        private var mEvents: List<Event>

        init {
            mOnClickListener = View.OnClickListener { v ->
                val event = v.tag as Event

                // Notify the active callbacks interface (the activity, if the fragment is attached to
                // one) that an item has been selected.
                mListener?.onSelectEvent(event)
            }
            ViewModelProviders.of(this@EventsFragment.activity!!, EventsFirebaseViewModelFactory()).get(EventsViewModel::class.java).events.let { list ->
                mEvents = list.value!!
                list.observe(this@EventsFragment, androidx.lifecycle.Observer { events ->
                    DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                        override fun getOldListSize(): Int {
                            return mEvents.size
                        }

                        override fun getNewListSize(): Int {
                            return events.size
                        }

                        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mEvents[oldItemPosition].event == events[newItemPosition].event
                        }

                        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mEvents[oldItemPosition] == events[newItemPosition]
                        }
                    }).also { mEvents = list.value!! }.dispatchUpdatesTo(this@EventsRecyclerViewAdapter)
                })
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val ui = EventItemUi(parent.context)
            return ViewHolder(ui.root, ui)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val event = mEvents[position]
            holder.ui.eventName = event.name

            val now = LocalDateTime.now()

            /*event.observe(this@EventsFragment, androidx.lifecycle.Observer { it ->
                if (it.years == null) return@Observer
                val yearSb = SpannableStringBuilder()
                val years = it.years?.map { year -> year.year }?.reversed()
                years?.forEachIndexed { index, s ->
                    val start = yearSb.length
                    yearSb.append(s)
                    if (s == now.year.toString()) {
                        yearSb.setSpan(StyleSpan(Typeface.BOLD), start, start + s.length, 0)
                    }
                    if (index != years.size - 1) yearSb.append(", ")
                }
                holder.yearsTextView.text = yearSb
            })
            event.value!!.fetchYears()*/

            //holder.text = ""

            /*val cal = Calendar.getInstance().apply { time = event.endTime }
            if (now.year <= cal[Calendar.YEAR] && now.monthValue <= cal[Calendar.MONTH] + 1 && now.dayOfMonth <= cal[Calendar.DAY_OF_MONTH]) {
                holder.nextMatchButton.text = context.getString(R.string.prev_match, event.prevMatch())
            } else {
                holder.nextMatchButton.visibility = View.INVISIBLE
            }*/

            with(holder.view) {
                tag = event
                setOnClickListener(mOnClickListener)
            }
        }

        override fun getItemCount(): Int = mEvents.size

        inner class ViewHolder(val view: View, val ui: EventItemUi) : RecyclerView.ViewHolder(view)
    }
}
