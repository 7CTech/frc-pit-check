package ctech.frcpitcheck.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem.SHOW_AS_ACTION_ALWAYS
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import ctech.frcpitcheck.BR
import ctech.frcpitcheck.R
import ctech.frcpitcheck.Util
import ctech.frcpitcheck.addOnPropertyChangedCallback
import ctech.frcpitcheck.model.Match
import ctech.frcpitcheck.ui.RecyclerViewFragmentDefaultUi
import ctech.frcpitcheck.ui.fragmentitems.MatchItemUi
import ctech.frcpitcheck.viewmodel.MatchesFirebaseViewModelFactory
import ctech.frcpitcheck.viewmodel.MatchesViewModel
import java.time.Clock
import java.time.LocalDateTime
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class MatchesFragment : Fragment() {
    private var listener: MatchesFragmentInteractionListener? = null
    private lateinit var mEvent: String
    private lateinit var mYear: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mEvent = arguments?.getString("event")!!
        mYear = arguments?.getString("year")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = RecyclerViewFragmentDefaultUi(inflater.context).root

        with(view) {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = MatchesRecyclerViewAdapter(listener)
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MatchesFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        if (menu == null) return
        menu.add(R.string.edit)
        menu.add(R.string.add).apply {
            setShowAsAction(SHOW_AS_ACTION_ALWAYS)
            setIcon(R.drawable.ic_add_white_24dp)
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface MatchesFragmentInteractionListener {
        fun onSelectMatch(match: Match?)
    }

    companion object {
        @JvmStatic
        fun newInstance(event: String, year: String): MatchesFragment {
            val args = Bundle().apply {
                putString("event", event)
                putString("year", year)
            }
            return MatchesFragment().apply {
                arguments = args
            }
        }
    }

    inner class MatchesRecyclerViewAdapter(
            private val mListener: MatchesFragment.MatchesFragmentInteractionListener?)
        : RecyclerView.Adapter<MatchesRecyclerViewAdapter.ViewHolder>() {


        private val mOnClickListener: View.OnClickListener
        private var mMatches: List<LiveData<Match>>

        init {
            mOnClickListener = View.OnClickListener { v ->
                val match = v.tag as Match
                // Notify the active callbacks interface (the activity, if the fragment is attached to
                // one) that an item has been selected.
                mListener?.onSelectMatch(match)
            }

            ViewModelProviders.of(this@MatchesFragment, MatchesFirebaseViewModelFactory(mEvent, mYear)).get(MatchesViewModel::class.java).matches.let { list ->
                mMatches = list.value!!
                list.observe(this@MatchesFragment, androidx.lifecycle.Observer { matchesLiveDataList ->
                    val matches = matchesLiveDataList.map { it.value!! }
                    DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                        override fun getOldListSize(): Int {
                            return mMatches.size
                        }

                        override fun getNewListSize(): Int {
                            return matches.size
                        }

                        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mMatches[oldItemPosition].value!!.event == matches[newItemPosition].event &&
                                    mMatches[oldItemPosition].value!!.year == matches[newItemPosition].year &&
                                    mMatches[oldItemPosition].value!!.match == matches[newItemPosition].match
                        }

                        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mMatches[oldItemPosition] == matches[newItemPosition]
                        }
                    }).also { mMatches = list.value!! }.dispatchUpdatesTo(this@MatchesRecyclerViewAdapter)
                })
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val ui = MatchItemUi(parent.context)
            return ViewHolder(ui.root, ui)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val matchLiveData = mMatches[position]
            val match = matchLiveData.value!!
            var matchTime = LocalDateTime.ofInstant(match.time.toInstant(), Clock.systemDefaultZone().zone) //everything is local to that event

            holder.ui.matchName = getString(R.string.match_number, match.match)
            var matchNumberRefresher: ScheduledFuture<*>? = null
            val refresher = {
                val timeBetween = Util.getTimeBetweenString(LocalDateTime.now(), matchTime, TimeUnit.MINUTES)
                this@MatchesFragment.activity?.runOnUiThread { holder.ui.matchTimeUntil = timeBetween.second }
                if (!timeBetween.first) matchNumberRefresher?.cancel(true)
            }
            Util.getTimeBetweenString(LocalDateTime.now(), matchTime, TimeUnit.MINUTES).let {
                holder.ui.matchTimeUntil = it.second
                if (it.first) {
                    refresher()
                    matchNumberRefresher = Util.executeAtInterval(refresher, 15, TimeUnit.SECONDS)
                    this@MatchesFragment.lifecycle.addObserver(object : DefaultLifecycleObserver {
                        override fun onResume(owner: LifecycleOwner) {
                            if (matchNumberRefresher == null) matchNumberRefresher = Util.executeAtInterval(refresher, 15, TimeUnit.SECONDS)
                        }

                        override fun onPause(owner: LifecycleOwner) {
                            matchNumberRefresher?.cancel(true)
                        }
                    })
                }
            }

            match.addOnPropertyChangedCallback { sender, propertyId ->
                when(propertyId) {
                    BR.time -> {
                        matchTime = LocalDateTime.ofInstant(match.time.toInstant(), Clock.systemDefaultZone().zone)
                        refresher()
                    }
                }
            }

            with(holder.view) {
                tag = match
                setOnClickListener(mOnClickListener)
            }
        }

        override fun getItemCount(): Int = mMatches.size

        inner class ViewHolder(val view: View, val ui: MatchItemUi) : RecyclerView.ViewHolder(view)
    }
}
