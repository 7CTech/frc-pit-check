package ctech.frcpitcheck.fragments

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import ctech.frcpitcheck.*
import ctech.frcpitcheck.model.Check
import ctech.frcpitcheck.ui.RecyclerViewFragmentDefaultUi
import ctech.frcpitcheck.ui.fragmentitems.CheckInputPassFailUi
import ctech.frcpitcheck.ui.fragmentitems.CheckItemUi
import ctech.frcpitcheck.ui.fragmentitems.CheckItemUnknownUi
import ctech.frcpitcheck.viewmodel.ChecksFirebaseViewModelFactory
import ctech.frcpitcheck.viewmodel.ChecksViewModel
import splitties.snackbar.snackForever

class ChecksFragment : Fragment() {
    private var mPrefs: SharedPreferences? = null
    private var mListener: ChecksFragmentInteractionListener? = null
    private var mRecyclerViewUi: RecyclerViewFragmentDefaultUi? = null
    private var mEditMode = false
    set(value) {
        mRecyclerViewUi?.isEnabled = value
        if (value) {
            activity?.findViewById<CoordinatorLayout>(R.id.coordinatorLayout)?.snackForever("Edit mode enabled") {
                setAction("Finish") {
                    mEditMode = false
                    dismiss()
                }
            }
        }
    }
    private lateinit var mEvent: String
    private lateinit var mYear: String
    private lateinit var mMatch: String
    private lateinit var mSubsystem: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mEvent = arguments?.getString("event")!!
        mYear = arguments?.getString("year")!!
        mMatch = arguments?.getString("match")!!
        mSubsystem = arguments?.getString("subsystem")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mRecyclerViewUi = RecyclerViewFragmentDefaultUi(inflater.context)
        with(mRecyclerViewUi!!.root) {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            adapter = ChecksRecyclerViewAdapter(mListener)
        }
        return mRecyclerViewUi!!.root
    }

    override fun onDestroyView() {
        mRecyclerViewUi = null
        super.onDestroyView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ChecksFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement ChecksFragmentInteractionListener")
        }
        mPrefs = activity!!.getPreferences(Activity.MODE_PRIVATE)
    }

    override fun onDetach() {
        super.onDetach()
        mPrefs = null
        mListener = null
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        if (menu == null) return
        menu.add(Menu.NONE, R.id.menuChecksEdit, Menu.NONE, R.string.edit).apply {
            setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
            setIcon(R.drawable.ic_edit_white_24dp)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item == null) return false
        return when (item.itemId) {
            R.id.menuChecksEdit -> {
                if (mPrefs?.getBoolean("showEditW", true) != false) {
                    Dialogs.editWarning(context!!, {
                        if (!it) mPrefs?.edit {
                            putBoolean("showEditW", false)
                            mEditMode = true
                        }
                    }, { })
                } else {
                    mEditMode = true
                }
                true
            }
            else -> false
        }
    }

    interface ChecksFragmentInteractionListener {
        fun onSelectCheck(check: Check?)
        fun onHoldCheck(check: Check?): Boolean
    }

    companion object {
        @JvmStatic
        fun newInstance(event: String, year: String, match: String, subsystem: String): ChecksFragment {
            val args = Bundle().apply {
                putString("event", event)
                putString("year", year)
                putString("match", match)
                putString("subsystem", subsystem)
            }
            return ChecksFragment().apply {
                arguments = args
            }
        }
    }

    inner class ChecksRecyclerViewAdapter(
            private val mListener: ChecksFragment.ChecksFragmentInteractionListener?)
        : RecyclerView.Adapter<ChecksRecyclerViewAdapter.ViewHolder>() {

        private val mOnClickListener: View.OnClickListener = View.OnClickListener { v ->
            val check = v.tag as Check
            mListener?.onSelectCheck(check)
        }
        private val mOnLongClickListener: View.OnLongClickListener = View.OnLongClickListener { v ->
            val check = v.tag as Check
            mListener?.onHoldCheck(check) ?: false
        }
        private var mChecks: List<LiveData<Check>>

        init {
            ViewModelProviders.of(this@ChecksFragment, ChecksFirebaseViewModelFactory(mEvent, mYear, mMatch, mSubsystem)).get(ChecksViewModel::class.java).checks.let { list ->
                mChecks = list.value!!
                list.observe(this@ChecksFragment, androidx.lifecycle.Observer { checksLiveDataList ->
                    val checks = checksLiveDataList.map { it.value!! }
                    DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                        override fun getOldListSize(): Int {
                            return mChecks.size
                        }

                        override fun getNewListSize(): Int {
                            return checks.size
                        }

                        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mChecks[oldItemPosition].value!!.event == checks[newItemPosition].event &&
                                    mChecks[oldItemPosition].value!!.year == checks[newItemPosition].year &&
                                    mChecks[oldItemPosition].value!!.match == checks[newItemPosition].match &&
                                    mChecks[oldItemPosition].value!!.subsystem == checks[newItemPosition].subsystem &&
                                    mChecks[oldItemPosition].value!!.check == checks[newItemPosition].check
                        }

                        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                            return mChecks[oldItemPosition] == checks[newItemPosition]
                        }
                    }).also { mChecks = list.value!! }.dispatchUpdatesTo(this@ChecksRecyclerViewAdapter)
                })
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val ui = CheckItemUi(parent.context)
            return ViewHolder(ui.root, ui)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val checkLiveData = mChecks[position]
            val check = checkLiveData.value!!
            holder.ui.checkName = check.name
            holder.ui.checkInput = when (check.type) {
                CheckType.UNKNOWN -> CheckItemUnknownUi(holder.view.context)
                CheckType.PASS_FAIL -> CheckInputPassFailUi(holder.view.context, check.result.toBoolean())
                CheckType.TICKBOX -> TODO()
                CheckType.COMMENT -> TODO()
            }

            check.addOnPropertyChangedCallback { sender, propertyId ->
                when(propertyId) {
                    BR.result -> {
                        holder.ui.result = check.result
                    }
                }
            }

            with(holder.view) {
                tag = check
                setOnClickListener(mOnClickListener)
                setOnLongClickListener(mOnLongClickListener)
            }
        }

        override fun getItemCount(): Int = mChecks.size

        inner class ViewHolder(val view: View, val ui: CheckItemUi) : RecyclerView.ViewHolder(view)
    }
}
