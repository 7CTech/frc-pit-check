package ctech.frcpitcheck

import android.app.AlertDialog
import android.content.Context
import android.widget.CheckBox
import androidx.core.view.marginBottom
import androidx.core.view.marginTop
import splitties.dimensions.dip
import splitties.views.dsl.core.*

object Dialogs {
    @JvmStatic
    fun authenticationFailed(context: Context, retryAuth: () -> Unit, killApp: () -> Unit): AlertDialog =
            AlertDialog.Builder(context)
                    .setTitle("Authentication Failed")
                    .setMessage("This app requires authentication with Google to function")
                    .setPositiveButton("Retry") { dialog, _ ->
                        retryAuth()
                        dialog.dismiss()
                    }
                    .setNegativeButton("Quit") { _, _ -> killApp() }
                    .show()
    @JvmStatic
    fun editWarning(context: Context, okCallback: (showAgain: Boolean) -> Unit, cancelCallback: () -> Unit): AlertDialog {
        val showAgainCheckBox: CheckBox
        return AlertDialog.Builder(context)
                .setTitle("Entering Edit Mode")
                .setMessage("You are about to enter edit mode. Changes made here will propagate to the database and other devices")
                .setView(context.frameLayout {
                    showAgainCheckBox = add(checkBox {
                        text = "Don't show this warning again"
                    }, lParams(width = matchParent, height = wrapContent) {
                        startMargin = dip(16)
                        topMargin = dip(8)
                        bottomMargin = dip(8)
                    })
                })
                .setPositiveButton("OK") { dialog, _ ->
                    okCallback(!showAgainCheckBox.isChecked)
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel") { _, _ -> cancelCallback() }
                .show()
    }
}