package ctech.frcpitcheck.ui

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.Menu
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.navigation.NavigationView
import ctech.frcpitcheck.R
import ctech.frcpitcheck.lib.attrDrawable
import splitties.resources.dimenPxSize
import splitties.resources.str
import splitties.views.dsl.appcompat.toolbar
import splitties.views.dsl.coordinatorlayout.appBarLParams
import splitties.views.dsl.coordinatorlayout.coordinatorLayout
import splitties.views.dsl.coordinatorlayout.defaultLParams
import splitties.views.dsl.core.*
import splitties.views.dsl.material.appBarLayout
import splitties.views.dsl.material.defaultLParams

class MainUi(override val ctx: Context) : Ui {
    val coordinatorLayout: CoordinatorLayout
    val navView: NavigationView
    val toolbar: Toolbar

    override val root = view(::DrawerLayout) {
        @SuppressLint("PrivateResource")
        coordinatorLayout = add(coordinatorLayout(id = R.id.coordinatorLayout) {
            add(appBarLayout(theme = R.style.AppTheme_AppBarOverlay) {
                toolbar = add(toolbar(theme = R.style.ThemeOverlay_AppCompat_Dark_ActionBar) {
                    background = attrDrawable(R.attr.colorPrimary)
                    popupTheme = R.style.AppTheme_PopupOverlay
                }, this@appBarLayout.defaultLParams(width = matchParent, height = dimenPxSize(R.dimen.abc_action_bar_default_height_material)))
            }, appBarLParams {
                behavior = AppBarLayout.Behavior().apply { setDragCallback(object : AppBarLayout.Behavior.DragCallback() {
                    override fun canDrag(p0: AppBarLayout): Boolean {
                        return false
                    }
                }) }
            })
            add(frameLayout(R.id.mainFragment), defaultLParams(matchParent, matchParent) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            })
        }, DrawerLayout.LayoutParams(matchParent, matchParent).also {
            it.gravity = Gravity.NO_GRAVITY
        })
        navView = add(view(::NavigationView) {
            menu.clear()
            menu.setGroupCheckable(0, true, true)
            menu.add(R.id.navGroup, R.id.navEvents, Menu.NONE, ctx.str(R.string.events)).apply {
                setIcon(R.drawable.ic_event_black)
                isChecked = true
            }
            menu.add(R.id.navGroup, R.id.navStatus, Menu.NONE, ctx.str(R.string.robot_status)).apply {
                setIcon(R.drawable.ic_assignment_black)
            }
            menu.add(R.id.navGroup, R.id.navNotify, Menu.NONE, ctx.str(R.string.notifications)).apply {
                setIcon(R.drawable.ic_notifications_black)
            }
            menu.add(R.id.navGroup, R.id.navAdmin, Menu.NONE, ctx.str(R.string.administration)).apply {
                setIcon(R.drawable.ic_build_black)
            }
            setNavigationItemSelectedListener {
                it.isChecked = true
                closeDrawers()
                true
            }
        }, DrawerLayout.LayoutParams(wrapContent, matchParent).also {
            fitsSystemWindows = true
            it.gravity = Gravity.START
        })
    }
}