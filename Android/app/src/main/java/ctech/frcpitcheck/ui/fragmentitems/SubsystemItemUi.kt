package ctech.frcpitcheck.ui.fragmentitems

import android.content.Context
import android.view.ViewGroup
import android.widget.TextView
import ctech.frcpitcheck.NO_GETTER
import ctech.frcpitcheck.lib.attrDrawable
import ctech.frcpitcheck.noGetter
import splitties.dimensions.dip
import splitties.views.dsl.constraintlayout.centerInParent
import splitties.views.dsl.constraintlayout.constraintLayout
import splitties.views.dsl.constraintlayout.lParams
import splitties.views.dsl.core.*

class SubsystemItemUi(override val ctx: Context) : Ui {
    var subsystemName: String
        @Deprecated(NO_GETTER, level = DeprecationLevel.HIDDEN) get() = noGetter
    set(value) {
        subsystemNameTextView.text = value
    }

    private val subsystemNameTextView: TextView

    override val root = constraintLayout {
        layoutParams = ViewGroup.LayoutParams(matchParent, dip(96))
        background = attrDrawable(android.R.attr.selectableItemBackground)

        subsystemNameTextView = add(textView {
            textSize = 24f
        }, lParams(wrapContent, wrapContent) {
            centerInParent()
            horizontalBias = 0f
            marginStart = dip(16)
        })
    }
}