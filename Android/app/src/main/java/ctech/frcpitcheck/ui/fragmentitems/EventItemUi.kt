package ctech.frcpitcheck.ui.fragmentitems

import android.content.Context
import android.view.ViewGroup
import android.widget.TextView
import ctech.frcpitcheck.NO_GETTER
import ctech.frcpitcheck.lib.attrDrawable
import ctech.frcpitcheck.noGetter
import splitties.dimensions.dip
import splitties.views.dsl.constraintlayout.*
import splitties.views.dsl.core.*

class EventItemUi(override val ctx: Context) : Ui {
    var eventName: String
        @Deprecated(NO_GETTER, level = DeprecationLevel.HIDDEN) get() = noGetter
    set(value) {
        eventNameTextView.text = value
    }

    private val eventNameTextView: TextView
    private val yearsTextView: TextView

    override val root = constraintLayout {
        layoutParams = ViewGroup.LayoutParams(matchParent, dip(96))
        background = attrDrawable(android.R.attr.selectableItemBackground)

        eventNameTextView = add(textView {
            textSize = 24f
        }, lParams(width = wrapContent, height = wrapContent) {
            centerHorizontally()
            horizontalBias = 0f
            marginStart = dip(16)
        })

        yearsTextView = add(textView {
            textSize = 20f
        }, lParams(width = wrapContent, height = dip(32)) { //todo: change height when view is actually populated
            centerHorizontally()
        })

        verticalChain(listOf(eventNameTextView, yearsTextView), packed, alignHorizontallyOnFirstView = true)
    }
}