package ctech.frcpitcheck.ui.fragmentitems

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ctech.frcpitcheck.NO_GETTER
import ctech.frcpitcheck.lib.attrDrawable
import ctech.frcpitcheck.noGetter
import splitties.dimensions.dip
import splitties.views.dsl.constraintlayout.*
import splitties.views.dsl.core.*

class MatchItemUi(override val ctx: Context) : Ui {
    var matchName: String
        @Deprecated(NO_GETTER, level = DeprecationLevel.HIDDEN) get() = noGetter
    set(value) {
        matchNameTextView.text = value
    }

    var matchTimeUntil: String
        @Deprecated(NO_GETTER, level = DeprecationLevel.HIDDEN) get() = noGetter
    set(value) {
        matchTimeUntilTextView.text = value
    }

    private val matchNameTextView: TextView
    private val matchTimeUntilTextView: TextView

    override val root = constraintLayout {
        layoutParams = ViewGroup.LayoutParams(matchParent, dip(96))
        background = attrDrawable(android.R.attr.selectableItemBackground)
        matchNameTextView = add(textView {
            textSize = 24f
        }, lParams(wrapContent, wrapContent) {
            centerVertically()
            marginStart = dip(16)
        })

        matchTimeUntilTextView = add(textView {
            textSize = 20f
        }, lParams(wrapContent, wrapContent) {
            marginEnd = dip(16)
        })

        horizontalChain(listOf(matchNameTextView, matchTimeUntilTextView), spreadInside, alignVerticallyOnFirstView = true)
    }
}