package ctech.frcpitcheck.ui

import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import ctech.frcpitcheck.ui.fragmentitems.CheckInputUi
import splitties.views.dsl.core.Ui
import splitties.views.dsl.core.matchParent
import splitties.views.dsl.core.wrapContent
import splitties.views.dsl.recyclerview.recyclerView

class RecyclerViewFragmentDefaultUi(override val ctx: Context): Ui {
    override val root = recyclerView {
        layoutManager = LinearLayoutManager(ctx)
        layoutParams = CoordinatorLayout.LayoutParams(matchParent, wrapContent)
    }
    var isEnabled: Boolean = false
    set(value) {
        for (i in 0 until (root.adapter?.itemCount ?: 0)) {
            (((root.layoutManager?.findViewByPosition(i) as ViewGroup?)?.getChildAt(1)!! as FrameLayout).getChildAt(0).tag as CheckInputUi).enabled = value
        }
        field = value
    }
}