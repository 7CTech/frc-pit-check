package ctech.frcpitcheck.ui.fragmentitems

import android.content.Context
import android.view.Gravity
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import ctech.frcpitcheck.NO_GETTER
import ctech.frcpitcheck.R
import ctech.frcpitcheck.lib.attrDrawable
import ctech.frcpitcheck.noGetter
import splitties.dimensions.dip
import splitties.views.dsl.appcompat.switch
import splitties.views.dsl.constraintlayout.*
import splitties.views.dsl.core.*
import splitties.views.textResource

class CheckItemUi(override val ctx: Context) : Ui {
    var checkName: String? = null
    set(value) {
        checkNameTextView.text = value
        field = value
    }

    var result: String = "0"
    set(value) {
        checkInput.result = value
    }

    var checkInput: CheckInputUi
    set(value) {
        checkInputFrameLayout.removeAllViews()
        checkInputFrameLayout.addView(value.root)
    }
    @Deprecated(NO_GETTER, level = DeprecationLevel.HIDDEN) get() = noGetter

    private val checkNameTextView: TextView
    private val checkInputFrameLayout: FrameLayout

    override val root = constraintLayout {
        layoutParams = ViewGroup.LayoutParams(matchParent, dip(96))
        background = attrDrawable(android.R.attr.selectableItemBackground)

        checkNameTextView = add(textView {
            textSize = 24f
        }, lParams(width = wrapContent, height = wrapContent) {
            centerVertically()
            marginStart = dip(16)
        })

        checkInputFrameLayout = add(frameLayout {

        }, lParams(width = wrapContent, height = matchParent) {
            centerVertically()
            marginEnd = dip(16)
        })

        horizontalChain(listOf(checkNameTextView, checkInputFrameLayout), spreadInside)
    }
}

interface CheckInputUi : Ui {
    var result: String
    var enabled: Boolean
}

class CheckInputPassFailUi(override val ctx: Context, pass: Boolean) : CheckInputUi {
    override var result: String
        get() = switch.isChecked.toString()
        set(value) {
            switch.isChecked = value.toBoolean()
        }

    override var enabled: Boolean
        get() = switch.isEnabled
        set(value) { switch.isEnabled = value }

    private val switch: SwitchCompat

    override val root = horizontalLayout {
        tag = this@CheckInputPassFailUi
        val text = add(textView {
            textResource = if (pass) R.string.pass else R.string.fail
            textSize = 18f
            gravity = Gravity.CENTER_VERTICAL or Gravity.END
        }, lParams {
            rightMargin = dip(8)
        })
        switch = add(switch {
            setOnCheckedChangeListener { _, isChecked ->
                text.textResource = if (isChecked) R.string.pass else R.string.fail
            }
            isChecked = pass
            isEnabled = false
        }, lParams {
            gravity = Gravity.CENTER_VERTICAL or Gravity.END
        })
    }
}

class CheckItemUnknownUi(override val ctx: Context) : CheckInputUi {
    override var result: String = ""
    override var enabled: Boolean = false
    override val root = textView {
        textResource = R.string.question_mark
        textSize = 30f
    }
}