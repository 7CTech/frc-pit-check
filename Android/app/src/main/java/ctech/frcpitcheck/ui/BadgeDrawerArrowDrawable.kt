package ctech.frcpitcheck.ui

import android.content.Context
import android.graphics.*
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable

class BadgeDrawerArrowDrawable(context: Context, text: String) : DrawerArrowDrawable(context) {

    private val mBackgroundPaint = Paint()
    private val mTextPaint = Paint()

    var backgroundColor: Int
    get() = mBackgroundPaint.color
    set(value) {
        if (mBackgroundPaint.color != value) {
            mBackgroundPaint.color = value
            invalidateSelf()
        }
    }

    var textColor: Int
    get() = mTextPaint.color
    set(value) {
        if (mTextPaint.color != value) {
            mTextPaint.color = value
            invalidateSelf()
        }
    }

    var text: String = text
    set(value) {
        if (field != value) {
            field = value
            invalidateSelf()
        }
    }

    var enabled = true
    set(value) {
        if (!enabled) {
            field = value
            invalidateSelf()
        }
    }

    init {
        mBackgroundPaint.color = Color.RED
        mBackgroundPaint.isAntiAlias = true

        mTextPaint.color = Color.WHITE
        mTextPaint.isAntiAlias = true
        mTextPaint.typeface = Typeface.DEFAULT_BOLD
        mTextPaint.textAlign = Paint.Align.CENTER
        mTextPaint.textSize = SIZE_FACTOR * intrinsicHeight
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        if (!enabled) return

        val bounds = bounds
        val x = (1 - HALF_SIZE_FACTOR) * bounds.width()
        val y = HALF_SIZE_FACTOR * bounds.height()
        canvas.drawCircle(x, y, SIZE_FACTOR * bounds.width(), mBackgroundPaint)

        if (text.isBlank()) return
        val textBounds = Rect()
        mTextPaint.getTextBounds(text, 0, text.length, textBounds)
        canvas.drawText(text, x, y + textBounds.height() / 2, mTextPaint)
    }

    companion object {
        @JvmStatic
        private val SIZE_FACTOR = 0.4f //todo: tweak
        @JvmStatic
        private val HALF_SIZE_FACTOR = SIZE_FACTOR / 2
    }
}