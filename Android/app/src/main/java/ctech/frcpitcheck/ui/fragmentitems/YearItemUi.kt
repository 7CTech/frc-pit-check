package ctech.frcpitcheck.ui.fragmentitems

import android.content.Context
import android.view.ViewGroup
import android.widget.TextView
import ctech.frcpitcheck.lib.attrDrawable
import splitties.dimensions.dip
import splitties.views.dsl.constraintlayout.centerInParent
import splitties.views.dsl.constraintlayout.constraintLayout
import splitties.views.dsl.constraintlayout.lParams
import splitties.views.dsl.core.*

class YearItemUi(override val ctx: Context) : Ui {
    var year: String
        @Deprecated(ctech.frcpitcheck.NO_GETTER, level = DeprecationLevel.HIDDEN) get() = ctech.frcpitcheck.noGetter
    set(value) {
        yearTextView.text = value
    }

    private val yearTextView: TextView

    override val root = constraintLayout {
        layoutParams = ViewGroup.LayoutParams(matchParent, dip(96))
        background = attrDrawable(android.R.attr.selectableItemBackground)

        yearTextView = add(textView {
            textSize = 24f
        }, lParams(width = wrapContent, height = wrapContent) {
            centerInParent()
            horizontalBias = 0f
            marginStart = dip(16)
        })
    }
}