package ctech.frcpitcheck.ui

import android.app.Activity
import android.content.Context
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import java.lang.Exception

class BadgeDrawerToggle : ActionBarDrawerToggle {
    private lateinit var badgeDrawable: BadgeDrawerArrowDrawable

    constructor(activity: Activity, drawerLayout: DrawerLayout,
                       @StringRes openDrawerContentDescRes: Int,
                       @StringRes closeDrawerContentDescRes: Int) : super(activity, drawerLayout, openDrawerContentDescRes, closeDrawerContentDescRes) {
        init(activity)
    }

    constructor(activity: Activity, drawerLayout: DrawerLayout,
                       toolbar: Toolbar, @StringRes openDrawerContentDescRes: Int,
                       @StringRes closeDrawerContentDescRes: Int) : super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes) {
        init(activity)
    }

    private fun init(activity: Activity) {
        val context = getThemedContext() ?: activity
        badgeDrawable = BadgeDrawerArrowDrawable(context, "")
        drawerArrowDrawable = badgeDrawable
    }

    var badgeEnabled: Boolean
    get() = badgeDrawable.enabled
    set(value) {
        badgeDrawable.enabled = value
    }

    var badgeText: String
    get() = badgeDrawable.text
    set(value) {
        badgeDrawable.text = value
    }

    var badgeColor: Int
    get() = badgeDrawable.backgroundColor
    set(value) {
        badgeDrawable.backgroundColor = value
    }

    var badgeTextColor: Int
    get() = badgeDrawable.textColor
    set(value) {
        badgeDrawable.textColor = value
    }

    private fun getThemedContext(): Context? {
        return try {
            val mActivityImpl = ActionBarDrawerToggle::class.java.getDeclaredField("mActivityImpl").apply { isAccessible = true }.get(this)
            mActivityImpl::class.java.getDeclaredMethod("getActionBarThemedContext").invoke(mActivityImpl) as Context
        } catch (e: Exception) {
            null
        }
    }
}