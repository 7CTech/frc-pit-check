package ctech.frcpitcheck.ui

import android.content.Context
import android.graphics.Typeface
import android.view.Gravity
import android.view.ViewGroup
import ctech.frcpitcheck.R
import splitties.dimensions.dip
import splitties.views.dsl.constraintlayout.*
import splitties.views.dsl.core.*
import splitties.views.dsl.core.styles.AndroidStyles

class LoadingUi(override val ctx: Context) : Ui {
    private val androidStyles = AndroidStyles(ctx)

    override val root = constraintLayout {
        layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        val progress = add(androidStyles.progressBar.default {
            isIndeterminate = true
        }, lParams(dip(120), dip(112)) {
            verticalBias = 0.45f
            centerHorizontally()
        })

        val loadingText = add(textView {
            text = ctx.getString(R.string.loading)
            textSize = 24f
            typeface = Typeface.DEFAULT_BOLD
            gravity = Gravity.CENTER
        }, lParams(wrapContent, wrapContent) {
            centerHorizontally()
        })

        verticalChain(listOf(progress, loadingText), packed)
    }
}