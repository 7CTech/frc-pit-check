package ctech.frcpitcheck.impl.firebase

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Source
import ctech.frcpitcheck.model.Event
import kotlinx.coroutines.tasks.await
import java.util.Date

class FirebaseEventImpl(private val ref: DocumentReference,
                        name: String, start: Date, end: Date) : Event() {
    private val mKey = ref.id
    private val mName: String = name
    private val mStart: Date = start
    private val mEnd: Date = end

    override val event: String
        get() = mKey
    override val name: String
        get() = mName
    override val startTime: Date
        get() = mStart
    override val endTime: Date
        get() = mEnd

    companion object {
        @JvmStatic
        fun fromFirebaseMap(ref: DocumentReference, map: Map<String, Any>) =
                FirebaseEventImpl(ref, map["name"] as String, (map["start"] as Timestamp).toDate(), (map["end"] as Timestamp).toDate())
        @JvmStatic
        suspend fun fromReference(ref: DocumentReference) =
                fromFirebaseMap(ref, ref.get().await().data!!)
    }
}