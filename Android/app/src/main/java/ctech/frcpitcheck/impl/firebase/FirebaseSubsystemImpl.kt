package ctech.frcpitcheck.impl.firebase

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Source
import ctech.frcpitcheck.model.Subsystem
import kotlinx.coroutines.tasks.await

class FirebaseSubsystemImpl(private val mEvent: String, private val mYear: String, private val mMatch: String, private val ref: DocumentReference,
                            name: String, owners: List<String>) : Subsystem() {
    private val mKey = ref.id
    private val mName: String = name
    private val mOwners: List<String> = owners

    override val event: String
        get() = mEvent
    override val year: String
        get() = mYear
    override val match: String
        get() = mMatch
    override val subsystem: String
        get() = mKey
    override val name: String
        get() = mName
    override val owners: List<String>
        get() = mOwners


    companion object {
        @JvmStatic
        fun fromFirebaseMap(event: String, year: String, match: String, ref: DocumentReference, map: Map<String, Any>) =
                FirebaseSubsystemImpl(event, year, match, ref, map["name"] as String, map["owners"] as List<String>)
        @JvmStatic
        suspend fun fromReference(event: String, year: String, match: String, ref: DocumentReference) =
                fromFirebaseMap(event, year, match, ref, ref.get().await().data!!)
    }
}