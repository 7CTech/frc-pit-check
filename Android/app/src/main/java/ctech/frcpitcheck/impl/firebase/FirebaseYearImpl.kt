package ctech.frcpitcheck.impl.firebase

import com.google.firebase.firestore.DocumentReference
import ctech.frcpitcheck.model.Year
import kotlinx.coroutines.tasks.await

class FirebaseYearImpl(private val mEvent: String, private val ref: DocumentReference,
                       robotKey: String) : Year() {
    private val mKey = ref.id

    init {
        this.robotKey = robotKey
    }

    override val event: String
        get() = mEvent
    override val year: String
        get() = mKey

    companion object {
        @JvmStatic
        fun fromFirebaseMap(event: String, ref: DocumentReference, map: Map<String, Any>): Year =
                FirebaseYearImpl(event, ref, map["robot_key"] as String)
        @JvmStatic
        suspend fun fromReference(event: String, ref: DocumentReference): Year =
                fromFirebaseMap(event, ref, ref.get().await().data!!)
    }
}