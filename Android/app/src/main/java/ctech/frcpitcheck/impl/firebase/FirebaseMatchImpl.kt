package ctech.frcpitcheck.impl.firebase

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import ctech.frcpitcheck.model.Match
import kotlinx.coroutines.tasks.await
import java.util.*

class FirebaseMatchImpl(private val mEvent: String, private val mYear: String, private val ref: DocumentReference,
                        time: Date) : Match(time) {
    private val mKey = ref.id

    override val event: String
        get() = mEvent
    override val year: String
        get() = mYear
    override val match: String
        get() = mKey

    companion object {
        @JvmStatic
        fun fromFirebaseMap(event: String, year: String, ref: DocumentReference, map: Map<String, Any>): Match =
                FirebaseMatchImpl(event, year, ref, (map["time"] as Timestamp).toDate())
        @JvmStatic
        suspend fun fromReference(event: String, year: String, ref: DocumentReference): Match =
                fromFirebaseMap(event, year, ref, ref.get().await().data!!)
    }
}