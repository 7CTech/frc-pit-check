package ctech.frcpitcheck.impl.firebase

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Source
import ctech.frcpitcheck.CheckType
import ctech.frcpitcheck.model.Check
import kotlinx.coroutines.tasks.await

class FirebaseCheckImpl(private val mEvent: String, private val mYear: String, private val mMatch: String, private val mSubsystem: String, private val ref: DocumentReference,
                        checkedBy: List<String>, description: String, name: String, result: String, type: CheckType) : Check(result) {

    private val mKey = ref.id
    private val mCheckedBy: List<String> = checkedBy
    private val mDescription: String = description
    private val mName: String = name
    private val mResult: String = result
    private val mType: CheckType = type

    override val event: String
        get() = mEvent
    override val year: String
        get() = mYear
    override val match: String
        get() = mMatch
    override val subsystem: String
        get() = mSubsystem
    override val check: String
        get() = mKey
    override val checkedBy: List<String>
        get() = mCheckedBy
    override val description: String
        get() = mDescription
    override val name: String
        get() = mName
    override val type: CheckType
        get() = mType

    companion object {
        @JvmStatic
        fun fromFirebaseMap(event: String, year: String, match: String, subsystem: String, ref: DocumentReference, map: Map<String, Any>): Check =
                FirebaseCheckImpl(event, year, match, subsystem, ref, map["checked_by"] as List<String>, map["description"] as String, map["name"] as String, map["result"] as String, CheckType.fromInt((map["type"] as Long).toInt()))
        @JvmStatic
        suspend fun fromReference(event: String, year: String, match: String, subsystem: String, ref: DocumentReference): Check =
                fromFirebaseMap(event, year, match, subsystem, ref, ref.get().await().data!!)
    }
}