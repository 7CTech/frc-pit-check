package ctech.frcpitcheck.impl.firebase

import com.google.firebase.auth.FirebaseUser
import ctech.frcpitcheck.interfaces.LoginUser
import kotlinx.coroutines.tasks.await

class FirebaseLoginUserImpl(private val mUser: FirebaseUser, private var customClaims: Map<String, Any>? = null) : LoginUser {
    override val hasAdmin: Boolean = customClaims?.get("a") as Boolean? ?: false
    override val uid: String = mUser.uid

    override val displayName: String = mUser.displayName!!
    override val email: String = mUser.email!!

    override suspend fun refresh() {
        customClaims = mUser.getIdToken(true).await().claims
    }
}