package ctech.frcpitcheck

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import ctech.frcpitcheck.fragments.*
import ctech.frcpitcheck.impl.firebase.FirebaseLoginUserImpl
import ctech.frcpitcheck.interfaces.AllFragmentListener
import ctech.frcpitcheck.model.*
import ctech.frcpitcheck.ui.BadgeDrawerToggle
import ctech.frcpitcheck.ui.MainUi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import splitties.snackbar.longSnack

class MainActivity : AppCompatActivity(), AllFragmentListener {
    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    private lateinit var mUser: ctech.frcpitcheck.interfaces.LoginUser

    private lateinit var mEventFragment: EventsFragment
    private var mActiveFragment: Fragment? = null

    private lateinit var mDrawerToggle: BadgeDrawerToggle

    private var mShouldRedrawOptions = true

    private val mInteraction = MainActivityInteraction()

    private lateinit var mainUi: MainUi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainUi = MainUi(this)
        setContentView(mainUi.root)
        setSupportActionBar(mainUi.toolbar)

        mDrawerToggle = BadgeDrawerToggle(this, mainUi.root, mainUi.toolbar, R.string.empty, R.string.empty)
        mDrawerToggle.badgeText = "1"

        /*supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_white)
        }*/

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.mainFragment, LoadingFragment())

        transaction.commit()

        mainUi.navView.setNavigationItemSelectedListener {
            it.isChecked = true
            mainUi.root.closeDrawers()
            true
        }

        val settings = FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build()
        FirebaseFirestore.getInstance().firestoreSettings = settings

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("1085534100033-9s6vfdbm5d120vden09ta6n9rn95esp0.apps.googleusercontent.com")
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        mAuth.currentUser.let {
            if (it == null) {
                startActivityForResult(mGoogleSignInClient.signInIntent, RC_SIGN_IN)
            } else {
                GlobalScope.launch { onSuccessfulSignin(FirebaseLoginUserImpl(it)) }
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        mDrawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if (mShouldRedrawOptions) {
            menu?.clear()
            mActiveFragment?.onPrepareOptionsMenu(menu)
        }
        mShouldRedrawOptions = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        mDrawerToggle.onOptionsItemSelected(item)
        return mActiveFragment?.onOptionsItemSelected(item) ?: super.onOptionsItemSelected(item)
    }

    override fun onSelectEvent(event: Event?) = mInteraction.onSelectEvent(event)
    override fun onSelectYear(year: Year?) = mInteraction.onSelectYear(year)
    override fun onSelectMatch(match: Match?) = mInteraction.onSelectMatch(match)
    override fun onSelectSubsystem(subsystem: Subsystem?) = mInteraction.onSelectSubsystem(subsystem)
    override fun onSelectCheck(check: Check?) = mInteraction.onSelectCheck(check)
    override fun onHoldCheck(check: Check?): Boolean = mInteraction.onHoldCheck(check)

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStackImmediate()
            val backstackEntry = supportFragmentManager.getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1)
            supportActionBar?.title = backstackEntry.breadCrumbTitle
            mShouldRedrawOptions = true
            mActiveFragment = supportFragmentManager.findFragmentByTag(backstackEntry.name)!!
            invalidateOptionsMenu()
        } else {
            killApp()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            RC_SIGN_IN -> {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)
                    firebaseAuthWithGoogle(account!!)
                } catch (e: Exception) {
                    Log.d(TAG, "Failed to authenticate: ", task.exception)
                    Dialogs.authenticationFailed(this, {
                        val signInIntent = mGoogleSignInClient.signInIntent
                        startActivityForResult(signInIntent, RC_SIGN_IN)
                    }, ::killApp).setOnCancelListener {
                        if (BuildConfig.DEBUG) GlobalScope.launch { onSuccessfulSignin(FirebaseLoginUserImpl(mAuth.currentUser!!)) }
                        else killApp()
                    }
                }
            }
        }
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        mAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful && mAuth.currentUser != null) {
                GlobalScope.launch { onSuccessfulSignin(FirebaseLoginUserImpl(mAuth.currentUser!!)) }
            } else {
                Log.e(TAG, "Failed to authenticate: ", it.exception)
                Dialogs.authenticationFailed(this, {
                    val signInIntent = mGoogleSignInClient.signInIntent
                    startActivityForResult(signInIntent, RC_SIGN_IN)
                }, ::killApp)
            }
        }

    }

    /**
     * Performs network ops - run async
     */
    private suspend fun onSuccessfulSignin(user: ctech.frcpitcheck.interfaces.LoginUser) {
        mUser = user
        mUser.refresh()
        mainUi.coordinatorLayout.longSnack(getString(R.string.signed_in_as, user.displayName, user.email))

        mEventFragment = EventsFragment.newInstance()

        runOnUiThread { supportActionBar?.title = getString(R.string.events) }

        supportFragmentManager.transaction {
            replace(R.id.mainFragment, mEventFragment, "e")
            setCustomAnimations(R.anim.scale_in_from_large, R.anim.scale_out_to_large)
            setBreadCrumbTitle(R.string.events)
            addToBackStack("e")
        }
    }

    private fun killApp() {
        finish()
    }

    companion object {
        private const val RC_SIGN_IN = 9001
        private const val TAG = "FRCPit"
    }

    inner class MainActivityInteraction : AllFragmentListener {
        override fun onSelectEvent(event: Event?) {
            if (event == null) return //we don't care
            mShouldRedrawOptions = true
            mActiveFragment = YearsFragment.newInstance(event.event)
            supportActionBar?.title = event.name
            supportFragmentManager.transaction {
                replace(R.id.mainFragment, mActiveFragment!!, "e/${event.event}/y")
                setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_left)
                setBreadCrumbTitle(event.name)
                addToBackStack("e/${event.event}/y")
            }
            invalidateOptionsMenu()
        }

        override fun onSelectYear(year: Year?) {
            if (year == null) return //we don't care
            mShouldRedrawOptions = true
            mActiveFragment = MatchesFragment.newInstance(year.event, year.year)
            supportActionBar?.title = "${year.event} (${year.year})"
            supportFragmentManager.transaction {
                replace(R.id.mainFragment, mActiveFragment!!, "e/${year.event}/${year.year}/m")
                setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_left)
                setBreadCrumbTitle("${year.event} (${year.year})")
                addToBackStack("e/${year.event}/${year.year}/m")
            }
            invalidateOptionsMenu()
        }

        override fun onSelectMatch(match: Match?) {
            if (match == null) return
            mShouldRedrawOptions = true
            mActiveFragment = SubsystemsFragment.newInstance(match.event, match.year, match.match)
            supportActionBar?.title = "${match.event} (${match.year}) - ${match.match}"
            supportFragmentManager.transaction {
                replace(R.id.mainFragment, mActiveFragment!!, "e/${match.event}/${match.year}/${match.match}/s")
                setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_left)
                setBreadCrumbTitle("${match.event} (${match.year}) - ${match.match}")
                addToBackStack("e/${match.event}/${match.year}/${match.match}/s")
            }
            invalidateOptionsMenu()
        }

        override fun onSelectSubsystem(subsystem: Subsystem?) {
            if (subsystem == null) return
            mShouldRedrawOptions = true
            mActiveFragment = ChecksFragment.newInstance(subsystem.event, subsystem.year, subsystem.match, subsystem.subsystem)
            supportActionBar?.title = "${subsystem.event} (${subsystem.year}) - ${subsystem.match}: ${subsystem.name}"
            supportFragmentManager.transaction {
                replace(R.id.mainFragment, mActiveFragment!!, "e/${subsystem.event}/${subsystem.year}/${subsystem.match}/${subsystem.subsystem}/c")
                setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_left)
                setBreadCrumbTitle("${subsystem.event} (${subsystem.year}) - ${subsystem.match}: ${subsystem.name}")
                addToBackStack("e/${subsystem.event}/${subsystem.year}/${subsystem.match}/${subsystem.subsystem}/c")
            }
            invalidateOptionsMenu()
        }

        override fun onSelectCheck(check: Check?) {
            if (check == null) return
        }

        override fun onHoldCheck(check: Check?): Boolean {
            if (check == null) return false
            return true
        }
    }
}
