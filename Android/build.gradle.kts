import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


buildscript {
    repositories {
        google()
        jcenter()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:3.4.1")
        classpath(kotlin("gradle-plugin", "1.3.11"))
        classpath("com.google.gms:google-services:4.2.0")

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

println("Kotlin Version: ${KotlinCompilerVersion.VERSION}")


allprojects {
    repositories {
        mavenLocal()
        google()
        jcenter()
    }

    val splittiesVersion by extra("3.0.0-alpha06")
    val splittiesSnapshotVersion by extra("3.0.0-alpha01-5")
}


tasks.create("clean", Delete::class.java) {
    delete(rootProject.buildDir)
}

tasks.getByName<Wrapper>("wrapper") {
    gradleVersion = "5.1.1"
    distributionType = org.gradle.api.tasks.wrapper.Wrapper.DistributionType.ALL
}

