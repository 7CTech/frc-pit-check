import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";

import {PermissionLevel, Subsystem} from "./frcpit";
import {DocumentSnapshot} from "firebase-functions/lib/providers/firestore";
import request = require('request-promise-native');
import {HttpsError} from "firebase-functions/lib/providers/https";
import {Http2ServerRequest} from "http2";


admin.initializeApp(functions.config().firebase);

async function get<T>(path: string, field: string = null): Promise<T> {
    if (field === null)
        return new Promise<T>((resolve, reject) => admin.firestore().doc(path).get().then((value: DocumentSnapshot) => resolve(value.data() as T), reject));
    else
        return new Promise<T>((resolve, reject) => admin.firestore().doc(path).get().then((value: DocumentSnapshot) => resolve(value.get(field) as T), reject));
}

async function set(path: string, value: any, field: string = null): Promise<admin.firestore.WriteResult> {
    let data = {};
    if (field === null)
        data = value;
    else
        data[field] = value;
    return new Promise<admin.firestore.WriteResult>((resolve, reject) => admin.firestore().doc(path).set(data).then(result => resolve(result), error => reject(error)));
}

async function update(path: string, value: any, field: string = null): Promise<admin.firestore.WriteResult> {
    let data = {};
    if (field === null)
        data = value;
    else
        data[field] = value;
    return new Promise<admin.firestore.WriteResult>((resolve, reject) => admin.firestore().doc(path).update(data).then(resolve, reject));
}

async function exists(path: string, field: string = null): Promise<boolean> {
    if (field === null)
        return new Promise<boolean>(resolve => admin.firestore().doc(path).get().then(value => resolve(value.exists), () => resolve(false)));
    else
        return new Promise<boolean>(resolve => admin.firestore().doc(path).get().then(value => resolve(value.get(field) !== undefined), () => resolve(false)));
}

async function checkUserHasPermission(uid: string, requestPermission: PermissionLevel) {
    return new Promise<boolean>(async (resolve) => {
        try {
            resolve(await get<number>(`users/${uid}`, "permLevel") >= requestPermission)
        } catch (e) {
            resolve(false)
        }
    });
}

async function getTokens(uid: string): Promise<string[]> {
    return get<string[]>(`/users/${uid}/fcm_tokens`);
}

/**
 *
 * @param data
 * @param context
 */
async function notify(data, context): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
        if (await checkUserHasPermission(context.auth.uid, PermissionLevel.NOTIFY)) {
            (await getTokens(data.target)).forEach(async token => {
                resolve(await admin.messaging().send({
                    token: token,
                    data: data.data
                }))
            });
        } else {
            reject("missing permissions")
        }
    });
}

function getReturnData(result: any, error: string = null): any {
    if (error === null) {
        return {
            data: result
        }
    } else {
        return {
            error: error,
            data: result
        }
    }
}

async function jsonRequest(url: string): Promise<any> {
    return await request(url, { json: true })
}
async function jsonRequestPost(url: string, token: string): Promise<any> {
    return await request.post({ headers: { Authorization: `Bearer $token` }, url: url, json: true})
}

export const createEvent = functions.https.onCall(async (data, context) => {
    if (await checkUserHasPermission(context.auth.uid, PermissionLevel.ADMIN)) {
        if (!await exists(`events/${data.year}/${data.eventCode}`)) {
            await set(`events/${data.eventCode}`, {name: data.name});
            return getReturnData(true);
        } else {
            return getReturnData(false, "event already exists")
        }
    } else {
        return getReturnData(false, "missing permissions")
    }
});

async function newRobotMatchData(year: string): Promise<Map<string, Subsystem>> {
    return get<Map<string, Subsystem>>(`robots/${year}/subsystems`)
}

export const createMatch = functions.https.onCall(async (data, context) => {
    if (await checkUserHasPermission(context.auth.uid, PermissionLevel.DRIVE)) {
        if (!await exists(`events/${data.year}/${data.eventCode}/${data.matchNum}`)) {
            await set(`events/${data.year}/${data.eventCode}/${data.matchNum}`, await newRobotMatchData(data.year));
            return getReturnData(`events/${data.year}/${data.eventCode}/${data.matchNum}`);
        }
    }
});

export const listMatches = functions.https.onCall(async (data, context) => {
    const claims = (await admin.auth().getUser(context.auth.uid)).customClaims;
    if (claims === null || claims === undefined || !(claims["r"])) {
        throw new HttpsError("permission-denied", "Missing permission 'read'");
    } else {
        return (await admin.firestore().collection("events").doc(data.event).collection(data.year).listDocuments()).map(obj => obj.id);
    }
});

export const listYears = functions.https.onCall(async (data, context) => {
    const claims = (await admin.auth().getUser(context.auth.uid)).customClaims;
    if (claims === null || claims === undefined || !(claims["r"])) {
        throw new HttpsError("permission-denied", "Missing permission 'read'");
    } else {
        return (await admin.firestore().collection("events").doc(data.event).listCollections()).map(collection => collection.id);
    }
});

export const listEvents = functions.https.onCall(async (data, context) => {
    const claims = (await admin.auth().getUser(context.auth.uid)).customClaims;
    console.log(claims);
    if (claims === null || claims === undefined || !(claims["r"])) {
        throw new HttpsError("permission-denied", "Missing permission 'read'");
    } else {
        return (await admin.firestore().collection("events").listDocuments()).map(obj => obj.id);
    }
});

/**
 * This function currently initialises the calling user
 * TODO initialise other users? how would we find the uid?
 * Params
 * - name: user's full name
 */
export const initialiseUser = functions.https.onCall(async (data, context) => {
    return exists(`users/${context.auth.uid}`).then(value => {
        if (value) {
            return getReturnData(false, "user already exists")
        } else {
            return set(`users/${context.auth.uid}`, data.name, "name").then(
                setResult => getReturnData(setResult !== null && setResult !== undefined),
                error => getReturnData(false, error.toString())
            );
        }
    }, error => getReturnData(false, error));
});