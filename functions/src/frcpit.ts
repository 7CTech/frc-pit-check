
export class User {
    name: string;
    role: PermissionLevel;
    fcm_tokens: string[];
}
/**
 * Various levels of permission, giving a person the ability to perform a certain operation
 * Permission levels are cumulative - granting a higher permission automagically grants lower ones
 */
export enum PermissionLevel {
    NONE = 0, //first cause null/0/blah
    READ = 1,

    NOTIFY = 7, //can send notifications to users to inspect/view/whatever - general messaging crap
    DRIVE = 8, //can report issues
    PIT = 9,//can mark subsystem functionality levels and report issues
    ADMIN = 10 //everything
}

export enum CheckType {
    //todo: this whole class
    PASS_FAIL = 0, //numeric constants are important cause multi platform
    TICKBOX = 1,
    COMMENT = 2,
}

export class Robot {
    name: string;
    subsystems: Map<string, Check>;
}

export class Subsystem {
    name: string;
    owners: string[];
    checkedBy: string;
    checks: Map<string, Check>;
}

export class Check {
    name: string;
    description: string;
    type: CheckType;
    result: any;
}

export class Match {
    subsystems: Map<string, Subsystem>;
    robot_key: string
}

export class Event {
    name: string;
}


